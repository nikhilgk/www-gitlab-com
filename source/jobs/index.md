---
layout: markdown_page
title: "Jobs"
---

## About GitLab

GitLab is quickly becoming the standard for source code version
management for any company, large or small. At GitLab, we help large
enterprises move to Git and build better software.

## Why work for GitLab?

GitLab is growing fast.
Working for GitLab means joining a very productive, ambitious team, where independence
and flexibility are both valued and required.
Your work will directly have a large impact on the present and future of GitLab.
We like to spend our time on things that matter.
We are a [remote only company](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
and you can work from wherever you want.
For more background please see our [about page](https://about.gitlab.com/about/),
our [culture page](https://about.gitlab.com/culture/), and our [handbook](https://about.gitlab.com/handbook/).

If you see yourself as a good fit with our company’s goals and team, then please
review the current job openings on this page, and submit your resume and cover
letter to be considered!

<div style="text-align: center">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/UTyXGx965Os" frameborder="0" allowfullscreen></iframe>
</div>

## We don't work with recruiters<a name="no-recruiters"></a>

We do not accept solicitations by recruiters, recruiting agencies, headhunters and outsourcing organizations.
If you email us we'll reply with [a link to this paragraph](https://about.gitlab.com/jobs/#no-recruiters) to indicate we would very much appreciate it if you stop emailing us and remove us from any marketing list.

## Available Openings

### Developer
* [Description](https://about.gitlab.com/jobs/developer/)
* [Apply](https://gitlab.workable.com/jobs/106660/candidates/new)
* As part of our technical interview<a name="technical-interview"></a>, you will
be asked to pick an issue from the GitLab CE issue tracker, and code 'live' with
the interviewer there to talk with and collaborate with. We do this because we
believe that it is the best way for you to see what the work is really like, and
for our [interviewer to see how you think, code, and collaborate](http://zachholman.com/posts/startup-interviewing-is-fucked/#collaborate).
When contributing code, you should follow the [Contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md),
and you agree to the [individual contributor license agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md).
If you prefer not to do the above please let us know and we'll give you an assignment that does not relate to GitLab but does test the relevant skills.
* If you get in the [contributors](http://contributors.gitlab.com/) top 10 we'll
  hire you (at least that is what has happened so far).
* We normally don't offer any internships but if you get a couple of merge requests
  accepted we'll interview you for one. This will be a remote internship without
  supervision, you'll only get feedback on your merge requests. If you want to
  work on open source and qualify please [submit an application](https://gitlab.workable.com/jobs/207439/candidates/new).
  In the cover letter field please note that you want an internship and link to
  the accepted merge requests. The merge requests should be of significant
  value and difficulty, which is at the discretion of the hiring manager. For
  example, fixing 10 typos isn't as valuable as shipping 2 new features.

### Product Marketing Manager

* [Description](https://about.gitlab.com/jobs/product-marketing-manager/)
* [Apply](https://gitlab.workable.com/jobs/202183/candidates/new)

### Service Engineer

* [Description](https://about.gitlab.com/jobs/service-engineer/)
* [Apply](https://gitlab.workable.com/jobs/87722/candidates/new)

### Production Engineer

* [Description](https://about.gitlab.com/jobs/production-engineer/)
* [Apply](https://gitlab.workable.com/jobs/142989/candidates/new)

### Account Manager

* [Description](https://about.gitlab.com/jobs/account-manager/)
* [Apply](https://gitlab.workable.com/jobs/88120/candidates/new)

### Senior Account Manager

* [Description](https://about.gitlab.com/jobs/account-manager/)
* [Apply](https://gitlab.workable.com/jobs/88117/candidates/new)

### People Operations Administrator

* [Description](https://about.gitlab.com/jobs/people-ops-administrator/)
* [Apply](https://gitlab.workable.com/jobs/220106/candidates/new)
