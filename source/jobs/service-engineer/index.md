---
layout: markdown_page
title: "Service Engineer"
---
The rising popularity of GitLab means that our professional services are in high demand.
If you have the skills to help our clients we would love to talk to you.

Currently we are looking for skilled people located in European and African timezones (UTC -2 to +4), as well as in Asian and Oceania timezones (UTC +5 to +10).
If you would love to work from home and help GitLab grow, this is the right spot for you.

## Responsibilities

* Engage with our customers—anything from a small advertising firm or a university, to Fortune 100 clients and 
help them with anything from a simple support ticket to a customer training
* Communicate via email and video conferencing with potential and current clients
* Maintain GitLab.com and our other services such as GitHost.io
* Participate in the rotating "on-call" list to provide 24/7 emergency response availability 
* Ensure that everything we learn from running GitLab.com is set as default or communicated to our users
* Write and update documentation based on customer interactions
* Submit and comment on feature requests based on customer interactions

More information can be found on the [support page in the handbook](https://about.gitlab.com/handbook/support/).

### Senior Service Engineer
Senior Service Engineers are experienced Service Engineers who 

* have deep knowledge of GitLab internals and a variety of possible configurations
* help hire and train new Service Engineers
* are a go-to person for the other Service Engineers when they face tough challenges
* improve the support process (Optimize Zendesk flow, suggest rake task additions)
* write blog articles 
* take ownership of documentation and feature requests that is based on customer interactions
* create merge requests to fix bugs


## Requirements for Applicants
(Check our [Jobs](https://about.gitlab.com/jobs/) page to see current openings).

* B.Sc. in Computer Science or equivalent experience
* Above average knowledge of Unix and Unix based Operating Systems
* Vast experience with Ruby on Rails Applications
* Good people skills
* Excellent spoken and written English
* Work remotely from anywhere in the world (Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)!)