---
layout: markdown_page
title: "Developer Relations"
---

The developer relations organization includes the following roles:  
- [Technical writing](/jobs/technical-writer/)  
- [Developer advocacy](/handbook/marketing/developer-relations/developer-advocacy/)  
- [Field marketing](/handbook/marketing/developer-relations/field-marketing/)  
- [Developer marketing](/handbook/marketing/developer-relations/developer-marketing/)  
